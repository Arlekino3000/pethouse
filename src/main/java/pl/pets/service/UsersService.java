package pl.pets.service;

import pl.pets.entity.Users;

import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
public interface UsersService {
    void add(String name, String secondName, String phone, String email, String login, String password);
    void edit(int id, String name, String secondName, String phone, String email, String login, String password);
    void delete(int id);
    Users findById(int id);
    List<Users> findAll();
    Users findByLogin(String login);
}
