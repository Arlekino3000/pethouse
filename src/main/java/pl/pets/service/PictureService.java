package pl.pets.service;

import pl.pets.entity.Picture;

import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
public interface PictureService {
    void add(String picturePath);
    void delete(int id, String picturePath);

    Picture findById(int id);
    List<Picture> findAll();
}
