package pl.pets.dao;

import pl.pets.entity.Users;

import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
public interface UsersDao {
    void add(Users users);
    void edit(Users users);
    void delete(Users users);
    Users findById(int id);
    List<Users>findAll();

    Users findByLogin(String login);
}
