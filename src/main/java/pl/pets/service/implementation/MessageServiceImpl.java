package pl.pets.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pets.dao.MessageDao;
import pl.pets.entity.Message;
import pl.pets.entity.Pets;
import pl.pets.entity.Users;
import pl.pets.service.MessageService;
import pl.pets.service.UsersService;

import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private UsersService usersService;
    @Autowired
    private UsersService usersDao;


    public void add(String messageText, Users users, Pets pets) {
//        Message text = new Message(messageText);
        Message message = new Message(messageText, users, pets);
        messageDao.add(message);
    }

    public void edit(int id, String messageText) {
        Message message = messageDao.findById(id);
        if (messageText != null && !messageText.equalsIgnoreCase(" ")){
            message.setMessageText(messageText);
        }messageDao.edit(message);
    }

    public void delete(int id) {
        messageDao.delete(messageDao.findById(id));
    }

    public Message findById(int id) {
        return messageDao.findById(id);
    }

    public List<Message> findAll() {
        return messageDao.findAll();
    }

    @Override
    public Message findByLogin(String login) {
        return messageDao.findByLogin(login);
    }

    @Override
    public Message findByUserId(int id) {
        return messageDao.findByUserId(id);
    }

    @Override
    public List<Message> showAllTheUser(int id) {
        return messageDao.showAllTheUser(id);
    }

    @Override
    public List<Message> showAllToPet(int id) {
        return messageDao.showAllToPet(id);
    }
}
