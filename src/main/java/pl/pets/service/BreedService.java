package pl.pets.service;

import pl.pets.entity.Breed;

import java.util.List;

/**
 * Created by Arlekino on 9/11/2017.
 */
public interface BreedService {
    void add(String name);
    void edit(int id, String name);
    void delete(int id);
    Breed findById(int id);
    List<Breed> findAll();
}
