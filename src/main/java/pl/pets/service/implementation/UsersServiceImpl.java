package pl.pets.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.UsersDao;
import pl.pets.entity.Users;
import pl.pets.service.UsersService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
@Transactional
@Service("userDetailsService")
public class UsersServiceImpl implements UsersService, UserDetailsService {
    @Autowired
    private UsersDao usersDao;


    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

    Users users = usersDao.findByLogin(login);
    List<SimpleGrantedAuthority>authorities = new ArrayList<SimpleGrantedAuthority>();
    // to write "ROLE_USER" ...
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return new User(users.getLogin(), users.getPassword(), authorities);
    }

    public void add(String name, String secondName, String phone, String email, String login, String password) {
        ShaPasswordEncoder encoder = new ShaPasswordEncoder();
        password= encoder.encodePassword(password,null);
        usersDao.add(new Users(name, secondName, phone, email, login, password));
    }

    public void edit(int id, String name, String secondName, String phone, String email, String login, String password) {
            Users users = usersDao.findById(id);
            if (name != null && !name.equalsIgnoreCase(" ")){
                users.setName(name);
            }
            if (secondName != null && !secondName.equalsIgnoreCase(" ")){
                users.setSecondName(secondName);
            }
            if (phone != null && !phone.equalsIgnoreCase(" ")){
                users.setPhone(phone);
            }
            if (email != null && !email.equalsIgnoreCase(" ")){
                users.setEmail(email);
            }
            if (login != null && !login.equalsIgnoreCase(" ")){
                users.setLogin(login);
            }
            if (password != null && !password.equals(" ")){
                users.setPassword(password);
            }
        }

    public void delete(int id) {
        usersDao.delete(usersDao.findById(id));
    }

    public Users findById(int id) {
        return usersDao.findById(id);
    }

    public List<Users> findAll() {
        return usersDao.findAll();
    }

    @Override
    public Users findByLogin(String login) {
        return usersDao.findByLogin(login);
    }

}

