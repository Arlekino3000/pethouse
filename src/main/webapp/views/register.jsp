<%--
  Created by IntelliJ IDEA.
  User: Arlekino
  Date: 9/15/2017
  Time: 8:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/css/style.css">
    <script src="/resources/js/jquery-2.0.3.min.js"></script>
    <script src="/resources/js/sstu_script.js"></script>
    <title>Title</title>
</head>
<body>
<div class="left_side">
    <h1 class="logo_text">
        <a href=""><img src="/resources/images/cat.jpg" height="150" width="150" alt="Тут має бути логотип"></a>
        <span>Final work</span>
    </h1>
    <div class="social_img">
        <a href="#" class="tw_icon"></a>
        <a href="#" class="go_icon"></a>
    </div>
    <div class="left_menu">
        <ul>
            <li><a href="/" id="link_top">Головна</a></li>
            <li><a href="/cat">Cats</a></li>
            <li><a href="/dog">Dogs</a></li>
            <li><a href="index.html_3">Мрія, мечта, dreams</a></li>

            <li><a href="/contact">Контакти</a></li>
        </ul>
    </div>
</div>
<a href="#" class="left_swap"></a>
<div class="wrapper">

    <p>То ж не тягни кота за хвіст, обирай собі друга! :-)</p>
    <img src="/resources/images/homeCatDog_1.jpg" alt="" class="auto_img">
    <form action="/register" method="post">

        <label for="name">Name:</label>
        <input name="name" id="name" type="text">

        <label for="secondName">Second name:</label>
        <input name="secondName" id="secondName" type="text">

        <label for="email">Email:</label>
        <input name="email" id="email" type="text">

        <label for="phone">Phone:</label>
        <input name="phone" id="phone" type="text">

        <label for="login">Login:</label>
        <input name="login" id="login" type="text">

        <label for="password">Password:</label>
        <input name="password" id="password" type="password">


        <button type="submit">Registration!</button>
    </form>


    <div class="footer">
        <div class="footer_block">
            <h4><a href="/dog">Кошки - Koty - Cats</a></h4>
            <img src="/resources/images/homeCat_2.jpg" alt="">
            <p>Ко́шка (лат. Félis silvéstris cátus), — домашнее животное, одно из наиболее популярных (наряду с собакой)
                «животных-компаньонов». </p>
        </div>
        <div class="footer_block">
            <h4><a href="/dog">Собаки-Psy-Dogs</a></h4>
            <img src="/resources/images/Dog_5.jpg" alt="">
            <p>Соба́ка (лат. Canis lupus familiaris) —
                Собаки известны своими способностями к обучению, любовью к хозяину, социальным поведением.</p>
        </div>
        <div class="footer_block">
            <h4><a href="/contact">Контакти</a></h4>
            <img src="/resources/images/DogCat_4.jpg" alt="">
            <p>ххххххххххххххххххххх <br>info@хххххххх; <br>Skype:хххххххх<br>tel:  (ххх) хххххххххх; (ххх) ххххххх</p>
        </div>
    </div>
</div>
</body>
</html>
