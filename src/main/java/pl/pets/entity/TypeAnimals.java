package pl.pets.entity;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Arlekino on 9/5/2017.
 */
@Entity
public class TypeAnimals {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @OneToMany(mappedBy = "typeAnimals")
    private List<Pets> petsList;

    public TypeAnimals() {
    }

    public TypeAnimals(String name) {
        this.name = name;
    }
    //    public TypeAnimals(String name, List<Pets> petsList) {
//        this.name = name;
//        this.petsList = petsList;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Pets> getPetsList() {
        return petsList;
    }

    public void setPetsList(List<Pets> petsList) {
        this.petsList = petsList;
    }
}
