package pl.pets.dao;

import pl.pets.entity.Pets;
import pl.pets.entity.TypeAnimals;

import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
public interface PetsDao {
    void add(Pets pets);

    void edit(Pets pets);

    Pets updatePhoto(int id, byte[] imageBytes);

    void delete(Pets pets);

    Pets findById(int id);
    Pets findType(String type);
    List<Pets> findCat();
    List<Pets> findDog();
    List<Pets> findAll();

    List<Pets> search(String name, Integer minAge, Integer maxAge);

    List<Pets> searchTyp(String type);

    List<Pets> searchType(TypeAnimals typeAnimals);
}
