<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="pets" uri="http://java.sun.com/jsp/jstl/core" %>


<%--
  Created by IntelliJ IDEA.
  User: Arlekino
  Date: 9/12/2017
  Time: 8:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/css/style.css">
    <script src="/resources/js/jquery-2.0.3.min.js"></script>
    <script src="/resources/js/sstu_script.js"></script>
    <title>Title</title>
</head>
<body>
<div class="left_side">
    <h1 class="logo_text">
        <a href=""><img src="/resources/images/cat.jpg" height="150" width="150" alt="Тут має бути логотип"></a>
        <span>Final work</span>
    </h1>
    <div class="social_img">
        <a href="#" class="tw_icon"></a>
        <a href="#" class="go_icon"></a>
    </div>
    <div class="left_menu">
        <ul>
            <li><a href="/" id="link_top">Головна</a></li>
            <li><a href="/cat">Cats</a></li>
            <li><a href="/dog">Dogs</a></li>
            <li><a href="/contact">Мрія, мечта, dreams</a></li>

            <li><a href="/contact">Контакти</a></li>
        </ul>
        <ul>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/petMessage/">Message</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <li><a href="/addNewPet">addPETS</a></li>
            </sec:authorize>
            <sec:authorize access="isAnonymous()">
                <li><a href="/loginpage">LOGIN</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="/logout">LOGOUT</a></li>
            </sec:authorize>
            <sec:authorize access="isAnonymous()">
                <li><a title="get more!" href="/register">REGISTRATION</a></li>
            </sec:authorize>
        </ul>
        <form action="/" method="get">
            <div class="search">
                <input placeholder="Enter name animal" name="searchKey">
                <button type="submit">Search</button>
            </div>
            <br>
            <div class="search">
                <span>Min    age:</span>
                <input type="number" name="minAge">
            </div>
            <br>
            <div class="search">
                <span>Max age:</span>
                <input type="number" name="maxAge">
            </div>
        </form>

    </div>
</div>

<a href="#" class="left_swap"></a>
<div class="wrapper">
    <div class="text_block">
        <h2>Здесь может быть ваша реклама</h2>

        <p>Любое домашнее животное благоприятно воздействует на психологическую атмосферу в вашем доме :-) </p>

        <p>Вы также можете выбрать себе личного четырехлапого психотерапевта, который всегда поймет, поддержит и утешит
            вас.</p>


        <p:forEach var="p" items="${pets}">
            <sec:authorize access="isAuthenticated()"><a href="/pets/${p.id}"></sec:authorize>

            <div class="boxPet">

                <img alt="img" src="data:image/jpeg;base64,${p.encodedImage}" class="auto_imgPet"/>
                <h1>${p.name}</h1>
                <p>${p.age}</p>

            </div>
            </a>

        </p:forEach>


        <p> сподіваюсь, що серед нашіх питомців ви знайдете собі когось до вподоби. Лише придивіться до цих фотографій
            - кожний погляд висловлює надію, кожен з них чекає на лагідну руку хазяїна</p>

    </div>
    <img src="/resources/images/homeCatDog_1.jpg" alt="" class="auto_img">
    <h1>То ж не тягни кота за хвіст, обирай собі друга! :-)</h1>
    <div class="footer">
        <div class="footer_block">
            <h4><a href="/cat">Кошки - Koty - Cats</a></h4>
            <img src="/resources/images/homeCat_2.jpg" alt="">
            <p>Ко́шка(лат. Félis silvéstris cátus), — домашнее животное, одно из наиболее популярных (наряду с собакой)
                «животных-компаньонов». </p>
        </div>
        <div class="footer_block">
            <h4><a href="/dog">Собаки-Psy-Dogs</a></h4>
            <img src="/resources/images/Dog_5.jpg" alt="">
            <p>Соба́ка (лат. Canis lupus familiaris) известны своими способностями к обучению, любовью к хозяину,
                социальным поведением.</p>
        </div>
        <div class="footer_block">
            <h4><a href="/contact">Контакти</a></h4>
            <img src="/resources/images/DogCat_4.jpg" alt="">
            <p>ххххххххххххххххххххх <br> info@хххххххх ; <br>Skype: хххххххх<br> tel: (ххх) ххххххх; (ххх) ххххххх</p>
        </div>
    </div>
</div>
</body>
</html>
