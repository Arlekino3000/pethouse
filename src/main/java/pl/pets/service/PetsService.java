package pl.pets.service;

import pl.pets.entity.Pets;
import pl.pets.entity.TypeAnimals;

import java.util.List;

/**
 * Created by Arlekino on 9/10/2017.
 */
public interface PetsService {
    void add(String name, String color, int age, String type, String description, byte[] imageBytes);
    void updatePhoto(int id, byte[] imageByte);
    void edit(int id, String name, String color, int age, String type, String description);
    void delete(int id);
    Pets findById(int id);
    List<Pets> findAll();
    List<Pets> findCat();
    List<Pets> findDog();
Pets findType(String type);
    List<Pets> search(String name, Integer minAge, Integer maxAge);

    List<Pets> searchTyp(String type);

    List<Pets> searchType(TypeAnimals typeAnimals);


}
