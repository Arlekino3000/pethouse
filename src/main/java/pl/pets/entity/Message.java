package pl.pets.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String messageText;
    @Column
    private Date date;
    @ManyToOne
    private Users users;
    @ManyToOne
    private Pets pets;

    public Message(String messageText, Users users, Pets pets) {
        this.messageText = messageText;
        this.users = users;
        this.pets = pets;
    }

    public Message(String messageText, Date date, Users users, Pets pets) {
        this.messageText = messageText;
        this.date = date;
        this.users = users;
        this.pets = pets;
    }

    public Message(String messageText) {
        this.messageText = messageText;
    }

    public Message() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Pets getPets() {
        return pets;
    }

    public void setPets(Pets pets) {
        this.pets = pets;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
