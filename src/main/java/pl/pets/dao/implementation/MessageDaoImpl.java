package pl.pets.dao.implementation;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.MessageDao;
import pl.pets.entity.Message;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
@Transactional
@Repository
public class MessageDaoImpl implements MessageDao {
    @PersistenceContext
    private EntityManager entityManager;
    @Transactional
    public void add(Message message) {
        entityManager.persist(message);
    }
    @Transactional
    public void edit(Message message) {
    entityManager.merge(message);
    }
    @Transactional
    public void delete(Message message) {
    entityManager.remove(message);
    }
    @Transactional
    public Message findById(int id) {
        return entityManager.find(Message.class, id);
    }
    @Transactional
    public List<Message> findAll() {
        return entityManager.createQuery("SELECT c FROM Message c").getResultList();
    }
@Transactional
    @Override
    public Message findByUserId(int userId_id) {
        return entityManager.find(Message.class, userId_id);
    }
@Transactional
    @Override
    public Message findByLogin(String login) {
        return entityManager.find(Message.class, login);
    }
@Transactional
    @Override
    public List<Message> showAllTheUser(int id) {
        return entityManager.createQuery("SELECT m FROM Message m where m.users.id=:id").setParameter("id", id).getResultList();
    }
@Transactional
    @Override
    public List<Message> showAllToPet(int id) {
        return entityManager.createQuery("SELECT m FROM Message m where (m.pets.id = :id)").setParameter("id",id).getResultList();
    }


}
