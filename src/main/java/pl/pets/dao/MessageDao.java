package pl.pets.dao;

import pl.pets.entity.Message;

import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
public interface MessageDao {
    void add(Message message);
    void edit (Message message);
    void delete (Message message);

    Message findById(int id);
    List<Message> findAll();

    Message findByUserId(int id);
    Message findByLogin(String login);
    List<Message> showAllTheUser(int id);
    List<Message> showAllToPet(int id);
}
