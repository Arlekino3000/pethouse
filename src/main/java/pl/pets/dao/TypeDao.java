package pl.pets.dao;

import pl.pets.entity.TypeAnimals;

import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
public interface TypeDao {
    void add (TypeAnimals typeAnimals);
    void edit (TypeAnimals typeAnimals);
    void delete (TypeAnimals typeAnimals);

    TypeAnimals findById(int id);
    List<TypeAnimals>findAll();
}
