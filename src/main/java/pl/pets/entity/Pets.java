package pl.pets.entity;

import org.springframework.web.servlet.ModelAndView;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Arlekino on 9/5/2017.
 */

@Entity
public class Pets {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String color;
    @Column(nullable = false)
    private int age;
    @Column(nullable = false)
    private String type;
    @ManyToOne
    private Users users;
    @ManyToOne
    private TypeAnimals typeAnimals;
    @ManyToOne
    private Breed breed;
    @Column
    private String description;

    @Lob()
    private byte [] imageBytes;

    private transient String encodedImage;

//    private String base64image;

    @OneToMany(mappedBy = "pets")
    private List<Message> messageList;
    @ManyToOne
    private Picture picture;
    public Pets() {
    }

    public Pets(String name, String color, int age, String type, String description) {
        this.name = name;
        this.color = color;
        this.age = age;
        this.type = type;
        this.description = description;
    }

    public Pets(String name, String color, int age, String description, byte[] imageBytes) {
        this.name = name;
        this.color = color;
        this.age = age;
     //  this.users = users;
     //   this.typeAnimals = typeAnimals;
     //   this.breed = breed;
        this.description = description;
        this.imageBytes = imageBytes;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public TypeAnimals getTypeAnimals() {
        return typeAnimals;
    }

    public void setTypeAnimals(TypeAnimals typeAnimals) {
        this.typeAnimals = typeAnimals;
    }

    public Breed getBreed() {
        return breed;
    }

    public void setBreed(Breed breed) {
        this.breed = breed;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

//    public void setImageBytes(String originalFilename) {
//    }

//    public ModelAndView getBase64image() {
//        return base64image;
//    }
//
//    public void setBase64image() {
//        this.base64image = base64image;
//    }

//    public String getBase64image() {
//        return base64image;
//    }
//
//    public void setBase64image(String base64image) {
//        this.base64image = base64image;
//    }
}
