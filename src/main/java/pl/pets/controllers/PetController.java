package pl.pets.controllers;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import pl.pets.entity.Pets;
import pl.pets.service.PetsService;
import pl.pets.service.UsersService;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.List;

/**
 * Created by Arlekino on 9/26/2017.
 */
@Controller
public class PetController {
    @Autowired
    private UsersService usersService;
    @Autowired
    private PetsService petsService;

    @RequestMapping(value = "/addNewPet", method = RequestMethod.GET)
    public String addPet() {
        return "addNewPet";
    }

    @RequestMapping(value = "/newPet", method = RequestMethod.POST)
    public String addPets(
            @RequestParam(value = "name") String name,
            // @RequestParam(value = "petBreed") String breed,
            @RequestParam(value = "color") String color,
            @RequestParam(value = "age") int age,
            @RequestParam(value = "type") String type,
            @RequestParam(value = "description") String description) {
//        @RequestParam(value = "image", required = false)
//        MultipartFile image,
//        Principal principal)
//     throws IOException {
//            byte[] imageBytes = null;
//            if (image != null) {
//                imageBytes = image.getBytes();
//            }

        petsService.add(name, color, age, type, description, null);
        return "redirect:/";
    }

    @RequestMapping(value = "/editPets", method = RequestMethod.POST)
    public String editPets(@RequestParam("id") Integer id,
                           @RequestParam("name") String name,
                           @RequestParam("color") String color,
                           @RequestParam("age") int age,
                           @RequestParam("type") String type,
                           @RequestParam("description") String description
                           //                       @RequestParam"image" byte[] imageBytes
    ) {
        petsService.edit(id, name, color, age, type, description);
        return "redirect:/";
    }

    @RequestMapping(value = "/deletePets/{id}")
    public String deletePets(@PathVariable Integer id) {
        petsService.delete(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/pets/{id}", method = RequestMethod.GET)
    public String petsPage(@PathVariable Integer id, Model model) throws Exception {
        Pets pet = petsService.findById(id);

        if (pet == null) {
            // тут перекерувати на сторінку 404
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }

        if (pet.getImageBytes() != null) {
            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
        }

        model.addAttribute("pets", pet);
        return "pets";
    }

    @RequestMapping(value = "/pets/edit/{id}")
    public String editPets(@PathVariable Integer id, Model model) throws UnsupportedEncodingException {
        Pets pet = petsService.findById(id);

        if (pet == null) {
            // тут перекерувати на сторінку 404
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }

        if (pet.getImageBytes() != null) {
            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
        }
        model.addAttribute("pets", pet);
        return "petsEdit";
    }

    @RequestMapping(value = "/pets/{id}/image", method = RequestMethod.POST)
    public String addFoto(@PathVariable("id") int id,
                          @RequestParam("image") MultipartFile image) throws Exception {
        if (image == null || image.isEmpty()) {
            throw new Exception("Not empty image required");
        }

        byte[] bytes = image.getBytes();
        petsService.updatePhoto(id, bytes);

        return "redirect:/pets/edit/" + id;
    }
//    @RequestMapping(value = "/")
//    public String home(
//            @RequestParam(value = "searchKey", required = false) String searchKey,
//            @RequestParam(value = "minAge", required = false) Integer minAge,
//            @RequestParam(value = "maxAge", required = false) Integer maxAge,
//            //  @RequestParam(value = "typeAnimals", required = false) String typeAnimals,
////
//            Model model) throws Exception {
//
//        List<Pets> petsList = petsService.search(searchKey, minAge, maxAge);
//        for (Pets pet : petsList) {
//            if (pet == null || pet.getImageBytes() == null) continue;
//            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
//            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
//        }
//        model.addAttribute("pets", petsList);
//        return "home";
//    }


//    @RequestMapping(value = "/")
//    public String home(
//            @RequestParam(value = "searchKey", required = false) String searchKey,
//
//            Model model) throws Exception {
//
//        List<Pets> petsList = petsService.searchTyp(searchKey);
//        for (Pets pet : petsList) {
//            if (pet == null || pet.getImageBytes() == null) continue;
//            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
//            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
//        }
//        model.addAttribute("pets", petsList);
//        return "home";
//    }
//    @RequestMapping(value = "cat", method = RequestMethod.GET)
//public String searchCat(Principal principal, Model model){
//        Pets pets = petsService.findType(principal.g)
//}

}
