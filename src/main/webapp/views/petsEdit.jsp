<%--
  Created by IntelliJ IDEA.
  User: Arlekino
  Date: 9/15/2017
  Time: 11:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/css/style.css">
    <script src="/resources/js/jquery-2.0.3.min.js"></script>
    <script src="/resources/js/sstu_script.js"></script>
    <title>Title</title>
</head>
<body>
<div class="left_side">
    <h1 class="logo_text">
        <a href=""><img src="/resources/images/cat.jpg" height="150" width="150" alt="Тут має бути логотип"></a>
        <span>Final work</span>
    </h1>
    <div class="social_img">
        <a href="#" class="tw_icon"></a>
        <a href="#" class="go_icon"></a>
    </div>
    <div class="left_menu">
        <ul>
            <li><a href="/" id="link_top">Головна</a></li>
            <li><a href="/cat">Cats</a></li>
            <li><a href="/dog">Dogs</a></li>
            <li><a href="index.html_3">Мрія, мечта, dreams</a></li>

            <li><a href="/contact">Контакти</a></li>
        </ul>
    </div>
</div>
<a href="#" class="left_swap"></a>
<div class="wrapper">
    <div class="text_block">
        <form action="/editPets" method="post">
            <input name="id" id="id" type="text" value="${pets.id}" hidden>
            <label for="name"></label>
            <input name="name" id="name" type="text" value="${pets.name}">
            <label for="color"></label>
            <input name="color" id="color" type="text" value="${pets.color}">
            <label for="type"></label>
            <input name="type" id="type" type="text" value="${pets.type}" placeholder="Cat or Dog?">
            <label for="age"></label>
            <input name="age" id="age" type="number" value="${pets.age}">
            <label for="description"></label>
            <input name="description" id="description" type="text" value="${pets.description}">
            <button type="submit">Edit</button>
        </form>

        <div>
            <p>Add photo</p>
            <form action="/pets/${pets.id}/image" method="post" enctype="multipart/form-data">
                <img alt="img" src="data:image/jpeg;base64,${pets.encodedImage}" class="auto_foto"/>
                <label>Choose image:</label>
                <input name="image" type="file" multiple accept="image/*">
                <button type="submit">Enter!</button>
            </form>
        </div>

        <%--<c:forEach items="${pets}" var="q">--%>
        <%-- ЛИСТАТЬ ФОТО ???--%>
        <%--</c:forEach>--%>
        <%--&lt;%&ndash;<h2>Здесь может быть ваша реклама</h2>&ndash;%&gt;--%>

        <%--&lt;%&ndash;<h3>Наши питомцы</h3>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<p>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<img src="images/DogCat_2.jpg" alt="" class="auto_img">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<c:forEach var="c" items="${commodities}">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<a href="/commodity/${c.id}">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<div class="box">&ndash;%&gt;--%>
        <%--&lt;%&ndash;<h2>${c.name}</h2>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<p>${c.price}</p>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<p><a href="/commodity/edit/${c.id}">Edit</a></p>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<p><a href="/deleteCommodity/${c.id}">Delete</a></p>&ndash;%&gt;--%>
        <%--</div>--%>
        <%--</a>--%>
        <%--</c:forEach>--%>
        <%--</p><br>--%>
        <p>То ж не тягни кота за хвіст, обирай собі друга! :-)</p>
        <img src="/resources/images/homeCatDog_1.jpg" alt="" class="auto_img">
    </div>


    <div class="footer">
        <div class="footer_block">
            <h4><a href="/cat">Кошки - Koty - Cats</a></h4>
            <img src="/resources/images/homeCat_2.jpg" alt="">
            <p>Ко́шка (лат. Félis silvéstris cátus), — домашнее животное, одно из наиболее популярных (наряду с собакой)
                «животных-компаньонов». </p>
        </div>
        <div class="footer_block">
            <h4><a href="/dog">Собаки-Psy-Dogs</a></h4>
            <img src="/resources/images/Dog_5.jpg" alt="">
            <p>Соба́ка (лат. Canis lupus familiaris) — Собаки известны своими способностями к обучению, любовью к хозяину, социальным поведением.</p>
        </div>
        <div class="footer_block">
            <h4><a href="/contact">Контакти</a></h4>
            <img src="/resources/images/DogCat_4.jpg" alt="">
            <p>ххххххххххххххххххххх <br>info@хххххххх; <br>Skype:хххххххх<br>tel:  (ххх) хххххххххх; (ххх) ххххххх</p>
        </div>
    </div>
</div>
</body>
</html>
