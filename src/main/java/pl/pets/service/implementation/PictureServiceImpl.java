package pl.pets.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pets.dao.PictureDao;
import pl.pets.entity.Picture;
import pl.pets.service.PictureService;

import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
@Service
public class PictureServiceImpl implements PictureService {
    @Autowired
    private PictureDao pictureDao;

    public void add(String picturePath) {
    Picture picture = new Picture(picturePath);
    pictureDao.add(picture);
    }

    public void delete(int id, String picturePath) {
    pictureDao.delete(pictureDao.findById(id));
    }

    public Picture findById(int id) {
        return pictureDao.findById(id);
    }

    public List<Picture> findAll() {
        return pictureDao.findAll();
    }
}
