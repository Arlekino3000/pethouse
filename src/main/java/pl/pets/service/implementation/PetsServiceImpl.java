package pl.pets.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pets.dao.PetsDao;
import pl.pets.entity.Pets;
import pl.pets.entity.TypeAnimals;
import pl.pets.service.PetsService;

import java.util.List;

/**
 * Created by Arlekino on 9/10/2017.
 */
@Service
public class PetsServiceImpl implements PetsService {
    @Autowired
    private PetsDao petsDao;


    public void add(String name, String color, int age, String type, String description, byte[] imageBytes) {
        Pets pets = new Pets(name, color, age, type, description);
        petsDao.add(pets);
    }
    public void updatePhoto(int id, byte[] imageBytes){
//        return petsDao.updatePhoto(id, imageBytes);
        Pets pets = petsDao.findById(id);
        pets.setImageBytes(imageBytes);
        petsDao.edit(pets);
    }

    public void edit(int id, String name, String color, int age, String type, String description) {
        Pets pets = petsDao.findById(id);
        if (name != null && !name.equalsIgnoreCase(" ")) {
            pets.setName(name);
        }
        if (color != null && !color.equalsIgnoreCase(" ")) {
            pets.setColor(color);
        }
        if (age != 0) {
            pets.setAge(age);
        }

        if (type != null && !type.equalsIgnoreCase(" ")){
            pets.setType(type);
        }
        if (description != null && !description.equalsIgnoreCase(" ")) {
            pets.setDescription(description);
        }
        petsDao.edit(pets);
    }

    public void delete(int id) {
        petsDao.delete(petsDao.findById(id));
    }

    public Pets findById(int id) {
        return petsDao.findById(id);
    }

    public Pets findType(String type){
        return petsDao.findType(type);
    }

    public List<Pets> findAll() {
        return petsDao.findAll();
    }

    @Override
    public List<Pets> findCat() {
        return petsDao.findCat();
    }

    @Override
    public List<Pets> findDog() {
        return petsDao.findDog();
    }

    public List<Pets> search(String name, Integer minAge, Integer maxAge) {
        return petsDao.search(name, minAge, maxAge);
    }


    @Override
    public List<Pets> searchTyp(String type) {
        return petsDao.searchTyp(type);
    }

    public List<Pets> searchType(TypeAnimals typeAnimals){
        return petsDao.searchType(typeAnimals);
    }
}
