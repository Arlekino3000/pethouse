<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="pets" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Arlekino
  Date: 9/13/2017
  Time: 7:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="/resources/css/style.css">
    <script src="/resources/js/jquery-2.0.3.min.js"></script>
    <script src="/resources/js/jquery-3.2.0.min.js"></script>
    <script src="/resources/js/sstu_script.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</head>
<body>
<div class="left_side">
    <h1 class="logo_text">
        <a href=""><img src="/resources/images/cat.jpg" height="150" width="150" alt="Тут має бути логотип"></a>
        <span>Final work</span>
    </h1>
    <div class="social_img">
        <a href="#" class="tw_icon"></a>
        <a href="#" class="go_icon"></a>
    </div>
    <div class="left_menu">
        <ul>
            <li><a href="/" id="link_top">Головна</a></li>
            <li><a href="/cat">Cats</a></li>
            <li><a href="/dog">Dogs</a></li>
            <li><a href="/dog">Мрія, мечта, dreams</a></li>

            <li><a href="/contact">Контакти</a></li>
        </ul>

        <ul>
            <sec:authorize access="isAuthenticated()"><li><a href="/petMessage/">Message</a></li></sec:authorize>
            <sec:authorize access="hasRole('ROLE_ADMIN')"><li><a href="/addNewPet">addPETS</a></li></sec:authorize>
            <sec:authorize access="isAnonymous()"><li><a href="/loginpage">LOGIN</a></li></sec:authorize>
            <sec:authorize access="isAuthenticated()"><li><a href="/logout">LOGOUT</a></li></sec:authorize>
            <sec:authorize access="isAnonymous()"><li><a href="/register">REGISTRATION</a></li></sec:authorize>
        </ul>
    </div>
</div>
<a href="#" class="left_swap"></a>
<div class="wrapper">
    <div class="text_block">

                           <img alt="img" src="data:image/jpeg;base64,${pets.encodedImage}" class="auto_foto"/>

                    <h2>${pets.name}</h2>
                    <p>${pets.age}</p>
                    <p>${pets.type}</p>
                    <p>${pets.description}</p>
                <sec:authorize access="hasRole('ROLE_ADMIN')"><p><a href="/pets/edit/${pets.id}">Edit</a></p></sec:authorize>
                <sec:authorize access="hasRole('ROLE_ADMIN')"><p><a href="/deletePets/${pets.id}">Delete</a></p></sec:authorize>
        <form action="/petMessage/${pets.id}" method="post">

            <textarea name="messageText" id="" cols="30" rows="10">Message</textarea>
            <button type="submit">Send</button>

        </form>
        <c:forEach var="q" items="${message}">
            <p>${q.getMessageText()}</p>
            <p>${q.getDate()}</p>
        </c:forEach>
        <h2>То ж не тягни кота за хвіст, обирай собі друга! :-)</h2>
        <img src="/resources/images/homeCatDog_1.jpg" alt="" class="auto_img">


    </div>

<div class="footer">
    <div class="footer_block">
        <h4><a href="/cat">Кошки - Koty - Cats</a></h4>
        <img src="/resources/images/homeCat_2.jpg" alt="">
        <p>Ко́шка (лат. Félis silvéstris cátus), — домашнее животное, одно из наиболее популярных (наряду с собакой) «животных-компаньонов». </p>
    </div>
    <div class="footer_block">
        <h4><a href="/dog">Собаки-Psy-Dogs</a></h4>
        <img src="/resources/images/Dog_5.jpg" alt="">
        <p>Соба́ка (лат. Canis lupus familiaris)
            Собаки известны своими способностями к обучению, любовью к хозяину, социальным поведением.</p>
    </div>
    <div class="footer_block">
        <h4><a href="/contact">Контакти</a></h4>
        <img src="/resources/images/DogCat_4.jpg" alt="">
        <p>ххххххххххххххххххххх <br>info@хххххххх; <br>Skype:хххххххх<br>(ххх) хххххххххх; (ххх) ххххххх</p>
    </div>
</div>
</div>
</body>
</html>
