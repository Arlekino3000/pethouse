package pl.pets.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.pets.entity.Message;
import pl.pets.service.MessageService;
import pl.pets.service.PetsService;
import pl.pets.service.UsersService;

import java.security.Principal;
import java.util.List;

/**
 * Created by Arlekino on 9/24/2017.
 */
@Controller
public class MessageController {
    @Autowired
    MessageService messageService;
    @Autowired
    UsersService usersService;
    @Autowired
    PetsService petsService;

    @RequestMapping(value = "/petMessage/{id}", method = RequestMethod.GET)
    public String petMessage(@PathVariable int id, Principal principal, Model model) {
        System.out.println(">>>>> Getting messages for pet: '" + id + "'");
        List<Message> messages = messageService.showAllTheUser(usersService.findByLogin(principal.getName()).getId());
        model.addAttribute("messageText", messages);
        for (Message message : messages) {
            System.out.println(message.getDate());
        }
        model.addAttribute("message", messages);
        return "pets";
    }
    @RequestMapping(value = "/petMessage/{id}", method = RequestMethod.POST)
    public String petMessage(@PathVariable int id,
                             @RequestParam(value = "messageText") String messageText, Principal principal) {
        messageService.add(messageText, usersService.findById(id), petsService.findById(id));
        return "redirect:/";
    }
}

class MessageBody {
    public String text;
}
