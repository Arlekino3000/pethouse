package pl.pets.dao.implementation;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.PictureDao;
import pl.pets.entity.Picture;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
@Repository
public class PictureDaoImpl implements PictureDao {
    @PersistenceContext(unitName = "Pets1")
    private EntityManager entityManager;
    @Transactional
    public void add(Picture picture) {
        entityManager.persist(picture);
    }
    @Transactional
    public void delete(Picture picture) {
    entityManager.remove(picture);
    }
    @Transactional
    public Picture findById(int id) {
        return entityManager.find(Picture.class, id);
    }
    @Transactional
    public List<Picture> findAll() {
        return entityManager.createQuery("SELECT c FROM Picture c").getResultList();
    }
}
