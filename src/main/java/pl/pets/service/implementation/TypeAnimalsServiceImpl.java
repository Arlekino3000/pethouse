package pl.pets.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pets.dao.TypeDao;
import pl.pets.entity.TypeAnimals;
import pl.pets.service.TypeAnimalsService;

import java.util.List;

/**
 * Created by Arlekino on 9/11/2017.
 */
@Service
public class TypeAnimalsServiceImpl implements TypeAnimalsService {
    @Autowired
    private TypeDao typeDao;

    public void add(String name) {
    TypeAnimals typeAnimals = new TypeAnimals(name);
    typeDao.add(typeAnimals);
    }

    public void edit(int id, String name) {
    TypeAnimals typeAnimals = typeDao.findById(id);
    if (name != null && !name.equalsIgnoreCase(" ")){
        typeAnimals.setName(name);
    }
    typeDao.edit(typeAnimals);
    }

    public void delete(int id) {
    typeDao.delete(typeDao.findById(id));
    }

    public TypeAnimals findById(int id) {
        return typeDao.findById(id);
    }

    public List<TypeAnimals> findAll() {
        return typeDao.findAll();
    }
}
