package pl.pets.dao.implementation;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.BreedDao;
import pl.pets.entity.Breed;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
@Repository
public class BreedDaoImpl implements BreedDao {
    @PersistenceContext(unitName = "Pets1")
    private EntityManager entityManager;
    @Transactional
    public void add(Breed breed) {
        entityManager.persist(breed);
    }
@Transactional
    public void edit(Breed breed) {
entityManager.merge(breed);
    }
@Transactional
    public void delete(Breed breed) {
entityManager.remove(breed);
    }
@Transactional
    public Breed findById(int id) {
        return entityManager.find(Breed.class, id);
    }
@Transactional
    public List<Breed> findAll() {
        return entityManager.createQuery("SELECT c FROM Breed c").getResultList();
    }
}
