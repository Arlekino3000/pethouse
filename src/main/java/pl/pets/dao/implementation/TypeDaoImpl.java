package pl.pets.dao.implementation;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.TypeDao;
import pl.pets.entity.TypeAnimals;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
@Repository
public class TypeDaoImpl implements TypeDao{
  @PersistenceContext(unitName = "Pets1")
    private EntityManager entityManager;
  @Transactional
    public void add(TypeAnimals typeAnimals) {
  entityManager.persist(typeAnimals);
    }
@Transactional
    public void edit(TypeAnimals typeAnimals) {
    entityManager.merge(typeAnimals);
    }
@Transactional
    public void delete(TypeAnimals typeAnimals) {
    entityManager.remove(typeAnimals);
    }
@Transactional
    public TypeAnimals findById(int id) {
        return entityManager.find(TypeAnimals.class, id);
    }
@Transactional
    public List<TypeAnimals> findAll() {
        return entityManager.createQuery("SELECT c FROM TypeAnimals c").getResultList();
    }
}
