package pl.pets.dao;

import pl.pets.entity.Breed;

import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
public interface BreedDao {
    void add (Breed breed);
    void edit (Breed breed);
    void delete (Breed breed);

    Breed findById(int id);
    List<Breed> findAll();
}
