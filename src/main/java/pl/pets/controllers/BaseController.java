package pl.pets.controllers;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import pl.pets.entity.Message;
import pl.pets.entity.Pets;
import pl.pets.service.MessageService;
import pl.pets.service.PetsService;
import pl.pets.service.UsersService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Arlekino on 9/12/2017.
 */
@Controller
public class BaseController {

    @Autowired
    private UsersService usersService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private PetsService petsService;

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact() {
        return "contact";
    }

    @RequestMapping(value = "/imageDisplay", method = RequestMethod.GET)
    public void showImage(@RequestParam(value = "id") Integer id, HttpServletResponse response, HttpServletRequest request)
            throws ServletException, IOException {
        Pets pets = petsService.findById(id);
//response.getContentType("image/jpeg");

        response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
        response.getOutputStream().write(pets.getImageBytes());

        response.getOutputStream().close();
    }
    @RequestMapping(value = "/cat", method = RequestMethod.GET)
    public String cat( Model model) throws Exception {

        List<Pets> petsList = petsService.findCat();
        for (Pets pet : petsList) {
            if (pet == null || pet.getImageBytes() == null) continue;
            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
        }
        model.addAttribute("pets", petsList);
        return "cat";
    }
    @RequestMapping(value = "/dog", method = RequestMethod.GET)
    public String dog( Model model) throws Exception {

        List<Pets> petsList = petsService.findDog();
        for (Pets pet : petsList) {
            if (pet == null || pet.getImageBytes() == null) continue;
            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
        }
        model.addAttribute("pets", petsList);
        return "dog";
    }
    @RequestMapping(value = "/")
    public String home(
            @RequestParam(value = "searchKey", required = false) String searchKey,
            @RequestParam(value = "minAge", required = false) Integer minAge,
            @RequestParam(value = "maxAge", required = false) Integer maxAge,
            //  @RequestParam(value = "typeAnimals", required = false) String typeAnimals,
//
            Model model) throws Exception {

        List<Pets> petsList = petsService.search(searchKey, minAge, maxAge);
        for (Pets pet : petsList) {
            if (pet == null || pet.getImageBytes() == null) continue;
            byte[] encodedBytes = Base64.encodeBase64(pet.getImageBytes());
            pet.setEncodedImage(new String(encodedBytes, "UTF-8"));
        }
        model.addAttribute("pets", petsList);
        return "home";
    }

    //    @RequestMapping(value = "/",method = RequestMethod.GET)
//    public String home(Principal principal, Model model)throws Exception{
//
//
//    List<Pets> petsList = petsService.findAll();
//        for (Pets pets : petsList) {
//            byte[] encodedBytes = Base64.encodeBase64(pets.getImageBytes());
//            pets.setEncodedImage(new String(encodedBytes, "UTF-8"));
//        }
//        model.addAttribute("pets", petsList);
//
//            Pets pets = (Pets) petsService.findAll();
//        model.addAttribute("pets", pets);
//////
////        List<Pets> petsArrayList = new ArrayList<Pets>();
////
//    //    List<Pets> petsList = petsService.findAll();
////
//        model.addAttribute("pets",pets);
////
//        return "home";
//    }
    @RequestMapping(value = "/galery")
    public String galery(Principal principal, Model model) {
        // Pets pets = (Pets) petsService.findById("id", id);
        List<Pets> petsList = petsService.findAll();
        model.addAttribute(petsList);
        return "redirect:/";
    }


    //      @PathVariable Model model
//      , @RequestParam("image") MultipartFile image
//        )
//            //throws Exception
//    {
//        for (Pets pets : petsService.findAll()) {
////            if (pets.getImageBytes() == null) {
////           //     throw new Exception("Not empty image required");
////            }else {
//                byte[] encodedBytes = Base64.encodeBase64(pets.getImageBytes());
//            //    pets.setEncodedImage(new String(encodedBytes, "UTF-8"));
////        Pets pets = (Pets) (Pets) petsService.findAll();
//                List<Pets> petsList = petsService.findAll();
//          //  }
////            pets = petsService.findAll();
//
//            model.addAttribute("pets", pets);
//    }
////            return "pets";
//        return "redirect:/";
//   }
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String registration() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registration(@RequestParam("name") String name,
                               @RequestParam("secondName") String secondName,
                               @RequestParam("email") String email,
                               @RequestParam("phone") String phone,
                               @RequestParam("login") String login,
                               @RequestParam("password") String password) {
        usersService.add(name, secondName, email, phone, login, password);
        return "redirect:/";
    }

    @RequestMapping(value = "/loginpage", method = RequestMethod.GET)
    public String loginpage() {

        return "login";
    }

//    @RequestMapping(value = "/")
//    public String home(@RequestParam(value = "typeAnimals", required = false) TypeAnimals typeAnimals,
//                       Model model){
//        List<Pets> petsList = petsService.searchType(typeAnimals);
//        model.addAttribute("pets", petsList);
//        return "home";
//    }

//    @RequestMapping(value = "/pets/{id}", method = RequestMethod.GET)
//    public String petsPage(@PathVariable Integer id, Model model) throws Exception {
//        Pets pets = petsService.findById(id);
//        byte[] encodedBytes = Base64.encodeBase64(pets.getImageBytes());
//        pets.setEncodedImage(new String(encodedBytes, "UTF-8"));
//        model.addAttribute("pets", pets);
//        return "pets";
//    }

//    @RequestMapping(value = "/addNewPet", method = RequestMethod.GET)
//    public String addPet() {
//        return "addNewPet";
//    }
//
//    @RequestMapping(value = "/newPet", method = RequestMethod.POST)
//    public String addPets(
//            @RequestParam(value = "name") String name,
//            // @RequestParam(value = "petBreed") String breed,
//            @RequestParam(value = "color") String color,
//            @RequestParam(value = "age") int age,
//            @RequestParam(value = "description") String description ) {
////                         @RequestParam(value = "image", required = false) MultipartFile image,
////                          Principal principal
////    ) throws IOException {
////        byte [] imageBytes = null;
////        if (image != null) {
////            imageBytes = image.getBytes();
////        }
//        // petsService.add("sobak", "", 2, "", imageBytes);
//        petsService.add(name, color, age, description, null);
//        return "redirect:/";
//    }

//    @RequestMapping(value = "/pets/edit/{id}")
//    public String editPets(@PathVariable Integer id, Model model) throws UnsupportedEncodingException {
//        Pets pets = petsService.findById(id);
//
//
//            if (pets.getImageBytes() != null){
//
//
//        byte[] encodedBytes = Base64.encodeBase64(pets.getImageBytes());
//    pets.setEncodedImage(new String(encodedBytes, "UTF-8")); }
//        model.addAttribute("pets", pets);
//        return "petsEdit";
//    }

//    @RequestMapping(value = "/editPets", method = RequestMethod.POST)
//    public String editPets(@RequestParam("id") Integer id,
//                           @RequestParam("name") String name,
//                           @RequestParam("color") String color,
//                           @RequestParam("age") int age,
//                           @RequestParam("description") String description) {
//        petsService.edit(id, name, color, age, description);
//        return "redirect:/";
//    }

//    @RequestMapping(value = "/deletePets/{id}")
//    public String deletePets(@PathVariable Integer id) {
//        petsService.delete(id);
//        return "redirect:/";
//    }
//
//    @RequestMapping(value = "/register", method = RequestMethod.GET)
//    public String registration() {
//        return "register";
//    }
//
//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public String registration(@RequestParam("name") String name,
//                               @RequestParam("secondName") String secondName,
//                               @RequestParam("email") String email,
//                               @RequestParam("phone") String phone,
//                               @RequestParam("login") String login,
//                               @RequestParam("password") String password) {
//        usersService.add(name, secondName, email, phone, login, password);
//        return "redirect:/";
//    }

//    @RequestMapping(value = "/loginpage", method = RequestMethod.GET)
//    public String loginpage() {
//        return "login";
//    }

//    @RequestMapping(value = "/petMessage/{id}", method = RequestMethod.GET)
//    public String petMessage(@PathVariable int id, Principal principal, Model model) {
//        List<Message> messages = messageService.showAllTheUser(usersService.findByLogin(principal.getName()).getId());
//        model.addAttribute("text", messages);
//        for (Message text : messages) {
//            System.out.println(text.getDate());
//        }
//        model.addAttribute("id", id);
//        return "petMessage";
//    }

//    @RequestMapping(value = "/petMessage/{id}", method = RequestMethod.POST)
//    public String petMessage(@PathVariable int id,
//                             @RequestParam(value = "messageText") String messageText, Principal principal) {
//        messageService.add(messageText, usersService.findById(id), petsService.findById(id));
//        return "redirect:/";
//    }

    //    @RequestMapping(value = "/photoPet", method = RequestMethod.GET)
//    public String addFotos(){
//        return "photoPet";
//    }
//    @RequestMapping(value = "/pets/{id}/image", method = RequestMethod.POST)
//    public String addFoto(@PathVariable("id") int id,
//                          @RequestParam("image") MultipartFile image) throws Exception {
//        if (image == null || image.isEmpty()) {
//            throw new Exception("Not empty image required");
//        }
//
//        byte[] bytes = image.getBytes();
//        petsService.updatePhoto(id, bytes);
//        return "redirect:/pets/edit/" + id;
//    }

    //            pets.setBase64image(base64Encoded);
}
