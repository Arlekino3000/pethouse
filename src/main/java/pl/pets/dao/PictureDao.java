package pl.pets.dao;

import pl.pets.entity.Picture;

import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
public interface PictureDao {
    void add(Picture picture);
    void delete(Picture picture);

    Picture findById(int id);
    List<Picture> findAll();
}
