package pl.pets.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pets.dao.BreedDao;
import pl.pets.entity.Breed;
import pl.pets.service.BreedService;

import java.util.List;

/**
 * Created by Arlekino on 9/11/2017.
 */
@Service
public class BreedServiceImpl implements BreedService {
    @Autowired
    private BreedDao breedDao;

    public void add(String name) {
     Breed breed = new Breed(name);
     breedDao.add(breed);
    }

    public void edit(int id, String name) {
    Breed breed = breedDao.findById(id);
    if(name != null && !name.equalsIgnoreCase(" ")){
       breed.setName(name);
    }
    breedDao.edit(breed);
    }

    public void delete(int id) {
    breedDao.delete(breedDao.findById(id));
    }

    public Breed findById(int id) {
        return breedDao.findById(id);
    }

    public List<Breed> findAll() {
        return breedDao.findAll();
    }
}
