package pl.pets.service;

import pl.pets.entity.Message;
import pl.pets.entity.Pets;
import pl.pets.entity.Users;

import java.util.List;

/**
 * Created by Arlekino on 9/15/2017.
 */
public interface MessageService {
    void add(String messageText, Users users, Pets pets);
    void edit(int id, String messageText);
    void delete(int id);

    Message findById(int id);
    List<Message> findAll();

    Message findByLogin(String login);
    Message findByUserId(int id);
    List<Message>showAllTheUser(int id);
    List<Message>showAllToPet(int id);
}
