package pl.pets.dao.implementation;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.UsersDao;
import pl.pets.entity.Users;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
@Repository
public class UsersDaoImpl implements UsersDao {
@PersistenceContext(unitName = "Pets1")
    private EntityManager entityManager;
@Transactional
    public void add(Users users) {
    entityManager.persist(users);
    }
@Transactional
    public void edit(Users users) {
    entityManager.merge(users);
    }
@Transactional
    public void delete(Users users) {
    entityManager.remove(users);
    }
@Transactional
    public Users findById(int id) {
        return entityManager.find(Users.class, id);
    }
@Transactional
    public List<Users> findAll() {
        return entityManager.createQuery("SELECT c FROM Users c").getResultList();
    }
@Transactional
    public Users findByLogin(String login) {
        return (Users) entityManager.createQuery("select c from Users  c where  c.login= :login").setParameter("login", login).getSingleResult();
    }
}
