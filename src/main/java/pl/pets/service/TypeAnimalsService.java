package pl.pets.service;

import pl.pets.entity.TypeAnimals;

import java.util.List;

/**
 * Created by Arlekino on 9/11/2017.
 */
public interface TypeAnimalsService {
    void add(String name);
    void edit(int id, String name);
    void delete(int id);
    TypeAnimals findById(int id);
    List<TypeAnimals> findAll();
}

