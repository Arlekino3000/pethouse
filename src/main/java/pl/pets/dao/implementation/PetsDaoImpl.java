package pl.pets.dao.implementation;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.pets.dao.PetsDao;
import pl.pets.entity.Pets;
import pl.pets.entity.TypeAnimals;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Arlekino on 9/6/2017.
 */
@Repository
public class PetsDaoImpl implements PetsDao {
    @PersistenceContext(unitName = "Pets1")
    private EntityManager entityManager;

    @Transactional
    public void add(Pets pets) {
        entityManager.persist(pets);
    }

    @Transactional
    public void edit(Pets pets) {
        entityManager.merge(pets);
    }

    @Modifying
    @Transactional
    public Pets updatePhoto(int id, byte[] imageBytes) {
        return (Pets) entityManager.createQuery("update Pets set imageBytes = :imageBytes where id = :id")
                .setParameter("imageBytes", imageBytes)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Transactional
    public void delete(Pets pets) {
//        entityManager.remove(pets);
        entityManager.remove(entityManager.contains(pets) ? pets : entityManager.merge(pets));
    }

    @Transactional
    public Pets findById(int id) {
        return entityManager.find(Pets.class, id);
    }

    public Pets findType(String type){
        return  entityManager.find(Pets.class, type);
    }

    @Transactional
    public List<Pets> findAll() {
        return entityManager.createQuery("SELECT c FROM Pets c").getResultList();
    }
@Transactional
    public List<Pets> findCat(){return entityManager.createQuery("SELECT p FROM Pets p where (p.type ='cat') ").getResultList();}
    @Transactional
    public List<Pets> findDog(){return entityManager.createQuery("SELECT p FROM Pets p where (p.type ='dog') ").getResultList();}
    @Transactional
    public List<Pets> search(String name, Integer minAge, Integer maxAge) {
        return entityManager.createQuery(
                "select p from Pets p where ((:name is null or p.name like CONCAT(:name, '%')) " +
                        "and (:minAge is null or p.age >= :minAge ) and (:maxAge is null or p.age <= :maxAge)) "
        )
                .setParameter("name", name)
                .setParameter("minAge", minAge)
                .setParameter("maxAge", maxAge)
                .getResultList();
    }
//    public List<Pets> searchCat(String type){
//        return entityManager.createQuery(
//                "SELECT c FROM Pets where (:type is  null or c.type like CONCAT(:type, '%') )")
//    }

    @Transactional
    public List<Pets> searchType(TypeAnimals typeAnimals) {
        return entityManager.createQuery("SELECT c FROM Pets where " +
                "(:typeAnimals is null or c.typeAnimals like CONCAT(:typeAnimals, '%') )")
                .setParameter("typeAnimals", typeAnimals).getResultList();
    }

    @Override
    @Transactional
    public List<Pets> searchTyp(String type) {
        return entityManager.createQuery(
                "SELECT t FROM Pets t where (:type is null or t.type like CONCAT(:type, '%'))"
        ).setParameter("type", type).getResultList();
    }

}
