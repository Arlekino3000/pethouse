<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Arlekino
  Date: 9/14/2017
  Time: 8:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/resources/css/style.css">
    <script src="/resources/js/jquery-2.0.3.min.js"></script>
    <script src="/resources/js/sstu_script.js"></script>
    <title>Title</title>
</head>
<body>
<div class="left_side">
    <h1 class="logo_text">
        <a href=""><img src="/resources/images/cat.jpg" height="150" width="150" alt="Тут має бути логотип"></a>
        <span>Final work</span>
    </h1>
    <div class="social_img">
        <a href="#" class="tw_icon"></a>
        <a href="#" class="go_icon"></a>
    </div>
    <div class="left_menu">
        <ul>
            <li><a href="/" id="link_top">Головна</a></li>
            <li><a href="/cat">Cats</a></li>
            <li><a href="/dog">Dogs</a></li>
            <li><a href="/contact">Мрія, мечта, dreams</a></li>

            <li><a href="/contact">Контакти</a></li>
        </ul>
    </div>
</div>
<a href="#" class="left_swap"></a>
<div class="wrapper">
    <div class="text_block">

       <form action="/newPet" method="post" enctype="multipart/form-data">
           <label for="name">Name:</label>
           <input name="name" id="name" type="text">
           <label for="age">Age:</label>
           <input name="age" id="age" type="text">
           <label for="color">Color:</label>
           <input name="color" id="color" type="text">
           <label for="type">choose type: Cat or Dog:</label>
           <input name="type" id="type" type="text">
           <label for="description">Description:</label>
           <input name="description" id="description" type="text">

        <button type="submit">Enter!</button>
       </form>

    </div>
    <p>То ж не тягни кота за хвіст, обирай собі друга! :-)</p>
    <img src="/resources/images/homeCatDog_1.jpg" alt="" class="auto_img">

    <div class="footer">
        <div class="footer_block">
            <h4><a href="/cat">Кошки - Koty - Cats</a></h4>
            <img src="/resources/images/homeCat_2.jpg" alt="">
            <p>Ко́шка (лат. Félis silvéstris cátus), — домашнее животное, одно из наиболее популярных (наряду с собакой) «животных-компаньонов». </p>
        </div>
        <div class="footer_block">
            <h4><a href="/dog">Собаки-Psy-Dogs</a></h4>
            <img src="/resources/images/Dog_5.jpg" alt="">
            <p>Соба́ка (лат. Canis lupus familiaris) — домашнее животное, одно из наиболее распространённых (наряду с кошкой) «животных-компаньонов».

                С зоологической точки зрения, собака — плацентарное млекопитающее отряда хищных семейства псовых.

                Собаки известны своими способностями к обучению, любовью к хозяину, социальным поведением.</p>
        </div>
        <div class="footer_block">
            <h4><a href="index_x.html">Контакти</a></h4>
            <img src="/resources/images/DogCat_4.jpg" alt="">
            <p>ххххххххххххххххххххх <br>info@хххххххх; <br>Skype:хххххххх<br>(ххх) хххххххххх; (ххх) ххххххх</p>
        </div>
    </div>
</div>
</body>
</html>
